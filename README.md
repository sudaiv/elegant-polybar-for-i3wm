# Polybar for i3wm

Polybar for i3wm with Weather, System Monitoring etc.

## Output:

#### Polybar Full

![Polybar-Full](Images/polybar-full3.png)

#### Polybar Left

![Polybar-Full](Images/polybar-left.png)
- Root `/` file system storage percentage
- CPU temperature
- RAM usage percentage
- CPU usage percentage
- Battery level
    * Charging animation
    * Discharging animation
    * Battery full 
- PulseAudio volume control with bar
    * Mute / Unmute on click
- Upload speed 
    * **Update your config with your default network adaptor**
- Download speed 
    * **Update your config with your default network adaptor**


#### Polybar Center

![Polybar-Full](Images/polybar-center.png)
- i3wm Workspace with icons

#### Polybar Right

![Polybar-Full](Images/polybar-right.png)
- Weather using `Weather-report`
    * **Update weather section of your config with your `fips` value for your location**
- Date / Time with alternatives on click
- Power Menu
    * Lock
    * Shutdown
    * Reboot
- System tray icons

## Requirements:

- Polybar (of course :p)
- Weather-report ([source](http://fungi.yuggoth.org/weather/) | [AUR](https://aur.archlinux.org/packages/weatherreport/))
    * Use following commands to get `fips` value for your current location. Please update the `fips` value in weather section of polybar config 
    *   `weather-report <Your-city-name>` like following example
        ```
        $ weather-report "New York"
        Searching via name...
        Your search is ambiguous, returning 13 matches:
           [fips2711146060] New York Mills city, MN
           [fips2746060] New York Mills city, MN
           [fips2902552400] New York township, MO
           [fips3118592115] New York precinct, NE
           [fips3401779610] West New York town, NJ
           [fips3479610] West New York town, NJ
           [fips36061] New York County, NY
           [fips3651000] New York city, NY
           [fips3651011] New York Mills village, NY
           [kjrb] New York [Downtown Manhattan/Wall St. Heliport], NY, US // New York Downtown Manhattan/wall St, NY, United States of America
           [klga] New York La Guardia Airport, NY, United States
           [knyc] New York City Central Park, NY, United States
           [nyz072] New York (Manhattan), NY

        ```
- Fonts
    * font-0 = "FiraCodeFont:style=Medium:size=12;3" (for Text)
    * font-1 = FontAwesome:style=Regular:size=15;4  (For Icons)


## Usage:

To use this Polybar use following commands 

```
git clone https://gitlab.com/Sudu1/elegant-polybar-for-i3wm.git
cd elegant-polybar-for-i3wm/
polybar -c config example
```

or start it from i3wm config (place [config](config) in your `~/.config/polybar/` directory)

```
exec_always --no-startup-id killall polybar;  polybar -c ~/.config/polybar/config example
```

## Notes:

- This polybar is primarily for i3wm but can be used with other window managers as well
- Please update the `fips` value in the weather section of polybar config using your favorite editor
- For the icons to show up in polybar i3 workspace, you need to have similar names in i3 config and polybar config like shown below
    * i3 config
        ```
        set $ws1 1:1
        set $ws2 2:2
        set $ws3 3:3
        set $ws4 4:4
        set $ws5 5:5
        set $ws6 6:6
        set $ws7 7:7
        set $ws8 8:8
        set $ws9 9:9
        set $ws10 0:0
        ```

    * Polybar config
        ```
        ws-icon-0 = 1; 
        ws-icon-1 = 2; 
        ws-icon-2 = 3;  
        ws-icon-3 = 4; 
        ws-icon-4 = 5; 
        ws-icon-5 = 6; 
        ws-icon-6 = 7; 
        ws-icon-7 = 8; 
        ws-icon-8 = 9; 
        ws-icon-9 = 0; 
        ws-icon-default = ♛
        ```


I hope you found this helpful. Stay safe and Enjoy 🎉

---

-Sudu
